
# Youtube Fetch API and Dashboard

An API to fetch latest videos from youtube sorted in reverse chronological order of their publishing date-time from YouTube for a given tag/search query in a paginated response.

## Stack Used

 - Backend : NodeJs and ExpressJs
 - Frontend : ReactJs and MaterialUI
 - Database : MongoDB Atlas

## How to Setup

 1. Clone this [project](https://gitlab.com/aayush.agarwal1936/fampay-youtube-assignment.git).
 2. Add the environment `.env` file inside `api` folder.
	 * API_KEY=key1/key2/key3 (keys separated by /)
	 * QUERY=your_search_query (cricket)
	 * MONGO_URL=your_mongo_url (attached in mail)
	 * PORT=your_port (4001)
3. Since this project is dockerised, your system should be compatible with docker.
4. Run `docker-compose build` to build the docker containers.
5. Run `docker-compose up -d` to start the docker.
6. Run `docker-compose logs -f` to view docker logs.
7. Once the docker is up, visit http://localhost:3000/ to view the simple dashboard. All the backend problem statement can be accessed from here as well.
	* `Next` takes you to the next page.
	* `Previous` takes you to the previous page.
	* `Refresh` reloads the data from the database and updates the state.
	* `Search` bar searches for any given search string which is fully/partially/orderly/unorderly present in the title/description of the data etc.
8.  To text the backend APIs directly,
	* `GET ALL DATA` : http://localhost:4001/api/getVideos?page=1
	* `SEARCH VIDEO` : http://localhost:4001/api/searchVideos?page=1&title=&description=
	* Pagination is implemented on basis of previous/next page with a limit of 3 results per page.
	*  Either of `title` or `description` is required in `query` for `SEARCH VIDEO API`
9. Run `docker-compose down` to stop docker.

## Dashboard Screenshot
![enter image description here](https://drive.google.com/uc?export=view&id=1UqUpTm8WGXzxtbdqFgh-vQ_qZJZX3GKn)

