/* eslint-disable no-undef */
const mongoose = require('mongoose');

const { Schema } = mongoose;

// Create Schema
const VideoSchema = new Schema({
  title: {
    type: String,
    required: true,
  },
  description: {
    type: String,
  },
  thumbnail: {
    type: String,
  },
  channelTitle: {
    type: String,
    required: true,
  },
  channelId: {
    type: String,
    required: true,
  },
  publishedAt: {
    type: Date,
    required: true,
  },
},
{
  versionKey: false,
});
VideoSchema.index({ description: 'text', title: 'text' });
module.exports = Videos = mongoose.model('videos', VideoSchema);
