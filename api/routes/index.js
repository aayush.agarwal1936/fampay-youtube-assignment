const express = require('express');

const router = express.Router();
const { getVideos, searchVideos } = require('@controllers');

router.get('/getVideos', getVideos);
router.get('/searchVideos', searchVideos);

module.exports = router;