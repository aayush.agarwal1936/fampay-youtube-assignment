const Videos = require('@models/Videos');

module.exports.getVideos = async (req, res) => {
    const limit = 3;
    const page = parseInt(req.query.page) || 1;
    const startIndex = (page - 1) * limit;
    const endIndex = page * limit;
    const docCount = await Videos.countDocuments().exec();
    const lastPage = Math.ceil(docCount / limit);

    // Check if page is valid
    if (page < 1 || page > lastPage) {
        res.status(404).json('Page Not Found');
    } else {
        const result = {};
        if (endIndex < docCount) result.nextPage = page + 1;
        if (startIndex > 0) result.previousPage = page - 1;
        try {
            result.results = await Videos.find().sort({ publishedAt: -1 }).limit(limit).skip(startIndex);
            res.json(result);
        } catch (e) {
            res.status(500).json({ message: e.message });
        }
    }
};

module.exports.searchVideos = async (req, res) => {
    const limit = 3;
    const { title = '', description = '' } = req.query
    const searchString = `${title} ${description}`
    const page = parseInt(req.query.page) || 1;
    const startIndex = (page - 1) * limit;
    const endIndex = page * limit;
    const docCount = await Videos.countDocuments({ $text: { $search: searchString }}).exec();
    const lastPage = Math.ceil(docCount / limit);

    // Check if page is valid
    if (page < 1 || page > lastPage) {
        res.status(404).json('Page Not Found');
    } else {
        const result = {};
        if (endIndex < docCount) result.nextPage = page + 1;
        if (startIndex > 0) result.previousPage = page - 1;
        try {
            result.results = await Videos.find({ $text: { $search: searchString }}).sort({ publishedAt: -1 }).limit(limit).skip(startIndex);
            res.json(result);
        } catch (e) {
            res.status(500).json({ message: e.message });
        }
    }
};