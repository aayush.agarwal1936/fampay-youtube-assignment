const express = require('express');
const { fetchYoutubeData } = require('./cron');
const mongoose = require('mongoose');
require('dotenv').config();
const routes = require('./routes');
const bodyParser = require('body-parser');

const app = express();
/* eslint-disable no-undef */
const { API_KEY, QUERY, MONGO_URL, PORT } = process.env;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Healthcheck
app.get('/', (req, res) => {
  res.send('Aok');
});

// Routes
app.use('/api', routes);

const port = PORT || 4001;

app.listen(port, () => console.log(`Server up and running on port ${port} !`));

// Connect to MongoDB
mongoose
  .connect(MONGO_URL, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log('MongoDB successfully connected'))
  .catch((err) => console.log(err));

// Cron to fetch data from youtube
fetchYoutubeData(API_KEY, QUERY);

