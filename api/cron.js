/* eslint-disable no-await-in-loop */
require('module-alias/register');
const CronJob = require('cron').CronJob;
const axios = require('axios');
const Videos = require('@models/Videos');

const checkAPIKey = (url) => {
    const data = axios.get(url);
    return data
}

module.exports.fetchYoutubeData = async (keys, QUERY) => {
    const job = new CronJob('0 * * * * *', async function () {
        const lastRequestTimeInMilliSeconds = Date.parse(new Date()) - (60 * 1000);
        const publishedAfter = new Date(lastRequestTimeInMilliSeconds).toISOString();
        const apiKeys = keys.split('/');
        for (const API_KEY of apiKeys) {
            const url = `https://www.googleapis.com/youtube/v3/search?key=${API_KEY}&type=video&part=snippet&publishedAfter=${publishedAfter}&maxResults=10&q=${QUERY}&order=date`;
            try {
                const response = await checkAPIKey(url);
                const data = response.data.items;
                data.forEach(async (item) => {
                    const { title, description, thumbnails, channelTitle, channelId, publishedAt } = item.snippet;
                    const video = new Videos({
                        title,
                        description,
                        thumbnail: thumbnails.default.url,
                        channelTitle,
                        channelId,
                        publishedAt,
                    });
                    const uploaded = await video.save();
                    if (uploaded) console.log('Uploaded');
                    else console.log('Failed to upload');
                });
                break;
            } catch (err) {
                console.log('API KEY quota finished. Trying new API KEY..');
            }
        }
    }, null, true, 'Asia/Kolkata');
    job.start();
}