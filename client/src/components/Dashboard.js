import React, { useState, useEffect } from 'react';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Avatar from '@mui/material/Avatar';
import Typography from '@mui/material/Typography';
import axios from 'axios';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import InputBase from '@mui/material/InputBase';
import SearchIcon from '@mui/icons-material/Search';
import RefreshIcon from '@mui/icons-material/Refresh';
import SkipPreviousIcon from '@mui/icons-material/SkipPrevious';
import SkipNextIcon from '@mui/icons-material/SkipNext';
import Paper from '@mui/material/Paper';
import Divider from '@mui/material/Divider';
import YouTubeIcon from '@mui/icons-material/YouTube';

function Dashboard(props) {

    const [tasks, setTasks] = useState([]);
    const [next, setNext] = useState(-1);
    const [previous, setPrevious] = useState(-1);
    const [searchValue, setSearchValue] = useState("");

    function getTasks() {
        axios
            .get("/api/getVideos?page=1")
            .then((res) => {
                console.log(res);
                setTasks(res.data.results);
                if (res.data.nextPage) setNext(res.data.nextPage);
                else setNext(-1);
                if (res.data.previousPage) setPrevious(res.data.previousPage);
                else setPrevious(-1)
                setSearchValue("");
            })
            .catch((e) => {
                console.log(e);
            });
    }

    const handleRefresh = () => {
        getTasks();
    }

    const handleNextPage = () => {
        const url = searchValue === "" ? `/api/getVideos?page=${next}`
        : `/api/searchVideos?page=${next}&title=${searchValue}`;
        axios
            .get(url)
            .then((res) => {
                console.log(res);
                setTasks(res.data.results);
                if (res.data.nextPage) setNext(res.data.nextPage);
                else setNext(-1);
                if (res.data.previousPage) setPrevious(res.data.previousPage);
                else setPrevious(-1)
            })
            .catch((e) => {
                console.log(e);
            });
    }

    const handlePreviousPage = () => {
        const url = searchValue === "" ? `/api/getVideos?page=${previous}`
        : `/api/searchVideos?page=${previous}&title=${searchValue}`;
        axios
            .get(url)
            .then((res) => {
                console.log(res);
                setTasks(res.data.results);
                if (res.data.nextPage) setNext(res.data.nextPage);
                else setNext(-1);
                if (res.data.previousPage) setPrevious(res.data.previousPage);
                else setPrevious(-1)
            })
            .catch((e) => {
                console.log(e);
            });
    }

    const handleSearch = () => {
        axios
            .get(`/api/searchVideos?title=${searchValue}`)
            .then((res) => {
                console.log(res);
                if (res.data.results) setTasks(res.data.results);
                else setTasks([]);
                if (res.data.nextPage) setNext(res.data.nextPage);
                else setNext(-1);
                if (res.data.previousPage) setPrevious(res.data.previousPage);
                else setPrevious(-1)
            })
            .catch((e) => {
                setTasks([]);
                console.log(e);
            });
    }

    const handleSearchBar = (event) => {
        setSearchValue(event.target.value);
    };

    useEffect(() => {
        getTasks();
    }, []);

    return (
        <>
            <Box sx={{ flexGrow: 1 }}>
                <AppBar position="static" sx={{backgroundColor: 'red'}}>
                    <Toolbar>
                        <Paper
                            component="form"
                            sx={{ p: '2px 4px', display: 'flex', alignItems: 'center', width: 600 }}
                        >
                            <InputBase
                                sx={{ ml: 1, flex: 1 }}
                                placeholder="Search Videos"
                                onChange={(event) => handleSearchBar(event)}
                            />
                            <IconButton sx={{ p: '10px' }}>
                                <SearchIcon onClick={handleSearch} />
                            </IconButton>

                            <Divider sx={{ height: 20, m: 0.5 }} orientation="vertical" />
                            <IconButton color="primary" sx={{ p: '10px' }} aria-label="directions">
                                <a href="https://www.youtube.com"><YouTubeIcon style={{ color: 'red' }} /></a>
                            </IconButton>
                        </Paper>
                        <Box sx={{ flexGrow: 1 }} />
                        <Box sx={{ display: { xs: 'none', md: 'flex' } }}>
                            <IconButton
                                size="small"
                                color="inherit"
                                onClick={handlePreviousPage}
                                disabled={previous === -1 ? true : false}
                            >
                                <SkipPreviousIcon /> &nbsp; Previous
                            </IconButton>
                            <IconButton
                                size="small"
                                color="inherit"
                                onClick={handleNextPage}
                                disabled={next === -1 ? true : false}
                            >
                                <SkipNextIcon /> &nbsp; Next
                            </IconButton>
                            <IconButton
                                size="small"
                                edge="end"
                                aria-haspopup="true"
                                onClick={handleRefresh}
                                color="inherit"
                            >
                                <RefreshIcon /> &nbsp; Refresh
                            </IconButton>
                        </Box>
                    </Toolbar>
                </AppBar>
            </Box>

            <List sx={{ width: '100%', maxWidth: 800, bgcolor: 'background.paper', marginLeft: 2, marginTop: 5 }}>
                {tasks.length === 0 ? "Search Not Found" : tasks.map((video, index) => (
                    <ListItem alignItems="flex-start" key={index}>
                        <ListItemAvatar>
                            <Avatar alt="V" variant="square" sx={{height: 150, width: 210}} src={video.thumbnail} />
                        </ListItemAvatar>
                        &nbsp;&nbsp;&nbsp;
                        <ListItemText
                            primary={video.title}
                            secondary={
                                <React.Fragment>
                                    <Typography
                                        sx={{ display: 'inline' }}
                                        component="span"
                                        variant="body2"
                                        color="text.primary"
                                    >
                                        {video.channelTitle}
                                    </Typography>
                                    {video.description}{"\n"}
                                    <Typography>
                                    {video.publishedAt.split('T')[0]}
                                    {" " + video.publishedAt.split('T')[1].split('.000Z')[0]}
                                    </Typography>
                                </React.Fragment>
                            }
                        />
                    </ListItem>
                ))}
            </List>
        </>
    )
}

export default Dashboard
